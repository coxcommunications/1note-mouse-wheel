#ifWinActive ahk_class Framework::CFrame
    WheelUp::
        ControlGetFocus, control, A
        Loop 3
        SendMessage, 0x115, 0, 0, %control%, A
    Return

#ifWinActive ahk_class Framework::CFrame
    WheelDown::
        ControlGetFocus, control, A
        Loop 3
        SendMessage, 0x115, 1, 0, %control%, A
    Return